package com.teamwork.qrcodereader.view;

public interface BasePresenter<V extends BaseView> {

    void onAttach(V targetView);

    void onDetach();
}

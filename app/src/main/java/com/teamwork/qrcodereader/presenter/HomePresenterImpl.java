package com.teamwork.qrcodereader.presenter;

import com.teamwork.qrcodereader.view.home.HomeView;

public class HomePresenterImpl implements HomePresenter {

    private HomeView view;

    @Override
    public void onAttach(HomeView targetView) {
        view = targetView;
    }




    @Override
    public void onDetach() {
        view = null;
    }
}

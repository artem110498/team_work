package com.teamwork.qrcodereader.presenter;

import com.teamwork.qrcodereader.view.BasePresenter;
import com.teamwork.qrcodereader.view.home.HomeView;

public interface HomePresenter extends BasePresenter<HomeView> {
}
